cmake_minimum_required(VERSION 3.19)
project(ktcn C CXX)

set(CMAKE_CXX_STANDARD 17)

include_directories(include)
file(GLOB APP_SOURCES source/*.cpp source/*.c)
# file(GLOB APP_HEADERS source/*.hpp source/*.h)
add_executable(ktcn ${APP_SOURCES})

# 添加链接库文件(lib)
# target_link_libraries(ktcn pancake)