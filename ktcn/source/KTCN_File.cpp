//
// Created by beanflame - 2021/11/12
//

#include <iostream>
#include <stdio.h>

using namespace std;

#include "KTCN_File.h"

string OpengFile(const char *file) {
    string data;
    FILE *fp = fopen(file, "r");
    if (!fp) {
        printf("%s Fail to open file!\n", file);
        exit(0);
    }
    while (!feof(fp)) {
        data += fgetc(fp);
    }
    fclose(fp);
    return data;
}

/*
void WritingFile(const char *file) {
    FILE *fp = fopen(file, "w+");
    //fprintf(fp, "This is testing for fprintf...\n");
    //fputs("This is testing for fputs...\n", fp);
    fclose(fp);
}
*/

void WritingFile(string file, string str) {
    FILE *fp = fopen(file.c_str(), "w+");
    //fprintf(fp, "This is testing for fprintf...\n");
    //fputs(str.c_str(), fp);

    fprintf(fp, str.c_str());

    fclose(fp);
}