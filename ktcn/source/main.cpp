//
// Created by beanflame - 2021/11/10
//

#include <stdio.h>
#include <cstring>
#include <ctime>


#include "KTCN_File.h"
#include "KTCN_string.h"
#include "KTCN_num.h"


int main(int argc, char *argv[]) {
    string args[argc];

    for (int i; i < argc; i++) {
        args[i] = argv[i];
    }

    Sprintf("----------------------------------------------\n");
    Sprintf("main.ktcn:\n");
    Sprintf(OpengFile("main.ktcn") + "\n");
    Sprintf("----------------------------------------------\n");
    Sprintf("命令行的参数:\n");
    for (int i; i < argc; i++) {
        Sprintf(Snum(i) + " : " + args[i] + "\n");
    }
    Sprintf("----------------------------------------------\n");


    for (int i; i < argc; i++) {
        if (args[i] == "ktcn") {
            Sprintf("ktcn ！\n");
        } else {
            Sprintf("命令错误\n");
        }
    }

    // 基于当前系统的当前日期/时间
    time_t now = time(0);
    tm *ltm = localtime(&now);
    // 输出 tm 结构的各个组成部分





    string year = Snum(1900 + ltm->tm_year);
    string moon = Snum(1 + ltm->tm_mon);
    string mday = Snum(ltm->tm_mday);
    string YMM = year + "-" + moon + "-" + mday;


    string hour = Snum(ltm->tm_hour);
    string min = Snum(ltm->tm_min);
    string sec = Snum(ltm->tm_sec);
    string HMS = hour + ":" + min + ":" + sec;


    WritingFile("logs/" + YMM + ".log", "[" + HMS + "] [main] [INFO]:");

    Sprintf("[" + HMS + "] [main] [INFO]:");


    // [11:25:08] [Client thread/INFO]:
    //WritingFile("logs/2021-11-12.log", "[11:23] [main] [INFO]:");
    // WritingFile("logs/2021-11-12.log", "[11:23] [main] [INFO]:");
    // WritingFile("logs/2021-11-12.log", "[11:23] [main] [INFO]:");


    return 0;
}