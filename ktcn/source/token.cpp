//
// Created by beanflame - 2021/10/24
//

#include <iostream>
#include <stdio.h>

using namespace std;


// identifier (标识符)
struct identifier {
    //主位函数类型
    const char *KTCN_MAIN = "main";    //主位函数
    //数据类型
    const char *KTCN_IMPORT = "import";    //导入
    const char *KTCN_NUM = "num";          //数据、数学？
    const char *KTCN_TEXT = "text";        //文本
    const char *KTCN_FUNC = "func";        //函数
    const char *KTCN_RETURN = "return";    //返回
    //布尔类型
    const char *KTCN_BOOL = "bool";        //开关
    const char *KTCN_TRUE = "true";        //开(1)
    const char *KTCN_FALSE = "false";      //关(0)
    const char *KTCN_NULL = "null";        //空(NULL)
    //判断类型
    const char *KTCN_IF = "if";            //如果
    const char *KTCN_ELSE = "else";        //否则
    const char *KTCN_ELSIF = "elsif";      //如果否则
    //循环类型
    const char *KTCN_FOR = "for";          //循环
    const char *KTCN_WHILE = "while";      //计数循环
    const char *KTCN_BREAK = "break";      //循环结束
};








